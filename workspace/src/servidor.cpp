//Autor: Marcos Velázquez Ruiz

#include "glut.h"
#include "MundoServidor.h"

//el único objeto global
CMundoServidor mundoServidor;

//los callback, funciones que serán llamadas automaticamente por la glut cuando sucedan eventos
//NO HACE FALTA LLAMARLAS EXPLICITAMENTE
void OnDraw(void); //esta función será llamada para dibujar
void OnTimer(int value); //esta función será llamada cuando transcurra una temporización
void OnKeyboardDown(unsigned char key, int x, int y); //cuando se pulse una tecla	

int main(int argc,char* argv[]) {
	//Inicializar el gestor de ventanas GLUT y crear la ventana
	glutInit(&argc, argv);
	glutInitWindowSize(800,600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("mundoServidor");

	//Registrar los callbacks
	glutDisplayFunc(OnDraw);
	//glutMouseFunc(OnRaton);

	glutTimerFunc(25,OnTimer,0); //le decimos que dentro de 25ms llame 1 vez a la funcion OnTimer()
	glutKeyboardFunc(OnKeyboardDown);
	glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);
	
	mundoServidor.InitGL();
	
	//pasarle el control a GLUT, que llamará a los callbacks
	glutMainLoop();	

	return 0;   
}

void OnDraw(void) {
	mundoServidor.OnDraw();
}

void OnTimer(int value) {
	mundoServidor.OnTimer(value);
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
}

void OnKeyboardDown(unsigned char key, int x, int y) {
	mundoServidor.OnKeyboardDown(key,x,y);
	glutPostRedisplay();
}

