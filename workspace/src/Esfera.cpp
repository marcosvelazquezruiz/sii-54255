// Esfera.cpp: implementation of the Esfera class.
//
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.25f;
	velocidad.x=10;
	velocidad.y=10;
	centro.x=0;
	centro.y=rand()/(float)RAND_MAX;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro = centro + velocidad*t;
	
	static int i=1;	

	if(i>0){
		radio+=0.01f;
		i++;
	}
	else if(i<0){
		radio -= 0.01f;
		i--;
	}
	
	if(i == 100) i=-1;
	if(i ==-100) i= 1;

}
