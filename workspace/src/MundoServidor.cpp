// MundoServidorServidor.cpp: implementation of the CMundoServidorServidor class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include "Puntuaciones.h"

#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h> //Write
#include <sys/mman.h> //mmap
			 //open
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#include <unistd.h>
#include <pthread.h>
#include <signal.h>


static void CerrarServidor(int n) {
	exit(n);
}

void* hilo_comandos(void* d) {
	CMundoServidor* p = (CMundoServidor*) d;
	p->RecibeComandosJugador();
}

void CMundoServidor::RecibeComandosJugador() {
	int n_read;
	//Apertura de la tubería cliente-servidor en modo escritura
	fifo_cliente_servidor = open("/tmp/FIFO_CLIENTE_SERVIDOR", O_RDONLY);
	if(fifo_cliente_servidor < 0) {
		perror("Error al abrir la tuberia FIFO_CLIENTE_SERVIDOR");
		exit(1);
	}
	while(1) {
		usleep(10);
		char cad[100];
		n_read = read(fifo_cliente_servidor, cad, sizeof(cad));
		if(n_read < 0) {
			perror("Error al leer el contenido de la tubería.");
			close(fifo_cliente_servidor);
			exit(1);
		}
		unsigned char key;
		sscanf(cad, "%c", &key);
		if(key == 's') jugador1.velocidad.y = -4;
		if(key == 'w') jugador1.velocidad.y = 4;
		if(key == 'l') jugador2.velocidad.y = -4;
		if(key == 'o') jugador2.velocidad.y = 4;
	}
}
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoServidor::CMundoServidor()
{
	Init();
	
}

CMundoServidor::~CMundoServidor()
{
	close(fd); //Se cierra la pipe
	if (unlink("/tmp/FIFO_LOGGER") < 0) {
		perror ("Error al eliminar /tmp/FIFO_LOGGER");
		exit(1);
	}
	close(fifo_servidor_cliente);
	close(fifo_cliente_servidor);
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	
	for(int i=0;i<paredes.size();i++){
		paredes[i].Dibuja();
		}


	for(int i=0;i<disparos.size();i++){
		disparos[i].Dibuja();
		}




	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	
	for(int i=0;i<esferas.size();i++){
		esferas[i].Dibuja();
		}
	for(int i=0;i<esferas.size();i++){
		esferas[i].Dibuja();
		}
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.050f);
	Puntuaciones puntos;
	
	for(int i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	for(int i=0;i<esferas.size();i++)
	{
		esferas[i].Mueve(0.050f);
	}
	
	for(int i=0;i<disparos.size();i++)
	{
		disparos[i].Mueve(0.050f);
	}
	
	for(int i=0;i<disparos.size();i++){
		if(jugador1.Rebota(disparos[i])){
			jugador1.Reducir();
			jugador2.Agrandar();
			disparos.erase(disparos.begin()+i);
			}
		else if(jugador2.Rebota(disparos[i])){
			jugador2.Reducir();
			jugador1.Agrandar();
			disparos.erase(disparos.begin()+i);
			}	
	}
	

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	
	for(int i=0;i<disparos.size();i++){
		if(fondo_izq.Rebota(disparos[i])){
			disparos.erase(disparos.begin()+i);
			}
		else if(fondo_dcho.Rebota(disparos[i])){
			disparos.erase(disparos.begin()+i);
			}	
	}
	
	for(int i=0;i <esferas.size();i++){
		for(int j=0;j<paredes.size();j++){
			paredes[j].Rebota(esferas[i]);
			}
		}
	
	for(int i=0;i <esferas.size();i++){
		jugador1.Rebota(esferas[i]);
		jugador2.Rebota(esferas[i]);	
		}
			
	for(int i=0; i<esferas.size();i++){
		if(fondo_izq.Rebota(esferas[i]))
		{
		esferas[i].centro.x=0;
		esferas[i].centro.y=rand()/(float)RAND_MAX;
		esferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
		esferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		puntos.jugador1 = puntos1;
		puntos.jugador2 = puntos2;
		puntos.lastWinner = 2;
		
		write(fd, &puntos, sizeof(puntos)); //Apunto el punto en la pipe & check errores
		
		
		}

		if(fondo_dcho.Rebota(esferas[i]))
		{
		esferas[i].centro.x=0;
		esferas[i].centro.y=rand()/(float)RAND_MAX;
		esferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
		puntos.jugador1 = puntos1;
		puntos.jugador2 = puntos2;
		puntos.lastWinner = 1;
		
		write(fd, &puntos, sizeof(puntos));
		
		}//Apunto el punto en la pipe
	}
	if(fondo_izq.Rebota(esfera))
		{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		puntos.jugador1 = puntos1;
		puntos.jugador2 = puntos2;
		puntos.lastWinner = 2;
		write(fd, &puntos, sizeof(puntos));
		
		}

	if(fondo_dcho.Rebota(esfera))
		{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		puntos.jugador1 = puntos1;
		puntos.jugador2 = puntos2;
		puntos.lastWinner = 1;
		write(fd, &puntos, sizeof(puntos));
		
		}
		
//Creación de la cadena que se envía del servidor al cliente con la información de las posiciones y los puntos
	char cad[200];
	sprintf(cad, "%f %f %f %f %f %f %f %f %f %f %f %d %d",
		esfera.centro.x, esfera.centro.y, esfera.radio,
		jugador1.x1, jugador1.y1, jugador1.x2, jugador1.y2,
		jugador2.x1, jugador2.y1, jugador2.x2, jugador2.y2,
		puntos1, puntos2);
	write(fifo_servidor_cliente, cad, sizeof(cad));
}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	
	case 'g':
		esferas.push_back(*(new Esfera));
		break;
	case 'h':
		if(esferas.size() > 0)
			esferas.erase(esferas.begin());
		break;
		
		
	case 'd':
		disparos.push_back(*(new Disparo(0,jugador1.getPos())));
		break;
	
	case 'k':
		disparos.push_back(*(new Disparo(1,jugador2.getPos())));
		break;

	}
}




void CMundoServidor::Init()
{
	fd = open("/tmp/FIFO_LOGGER", O_WRONLY);
	if (fd < 0) {
        	perror("No puede abrirse la tubería.");
        	exit(1);
    	}

    //Apertura de la tubería servidor-cliente en modo escritura
	fifo_servidor_cliente = open("/tmp/FIFO_SERVIDOR_CLIENTE", O_WRONLY);
	if (fifo_servidor_cliente < 0) {
		perror("Error al abrir la tuberia FIFO_SERVIDOR_CLIENTE");
		exit(1);
	}
	
	//Armado de la señal sigpipe
	struct sigaction accion;
	accion.sa_handler = &CerrarServidor;
	if(sigaction(SIGPIPE, &accion, NULL) < 0) {
		perror("sigaction");
		exit(1);
	}
	
	//Creación del thread
	pthread_attr_init(&atrib);
	pthread_attr_setdetachstate(&atrib, PTHREAD_CREATE_JOINABLE);
	pthread_create(&thid1, &atrib, hilo_comandos, this);
	
	Plano p;
	//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

	//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
