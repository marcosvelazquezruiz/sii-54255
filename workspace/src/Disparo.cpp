// Disparo.cpp: implementation of the Disparo class.
//
//////////////////////////////////////////////////////////////////////

#include "Disparo.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Disparo::Disparo(int s, Vector2D pos)
{
	radio=0.25f;
	centro=pos;
	sentido=s;
	velocidad.y=0;

	if(s==0){
		velocidad.x=3;
		centro.x+=0.2;
		}

	else{
		velocidad.x=-3;
		centro.x-=0.2;
		}




}

Disparo::~Disparo()
{

}



void Disparo::Dibuja()
{
	glColor3ub(0,255,150);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Disparo::Mueve(float t)
{
	centro = centro + velocidad*t;

}

