// Autor: Marcos Velazquez Ruiz
//
//Disparo.h: interface for the Disparo class.
//
//////////////////////////////////////////////////////////////////////

#ifndef DISPARO_H
#define DISPARO_H


#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Vector2D.h"
#include <fstream>

class Disparo  {
private:
	int sentido; // 0=Rigth , 1=Left
public:	
	Disparo(int s, Vector2D pos);
	virtual ~Disparo();
		
	Vector2D centro;
	Vector2D velocidad;
	float radio;

	void Mueve(float t);
	void Dibuja();
};

#endif 
